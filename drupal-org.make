; drupal_accounting make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[admin_views][version] = "1.6"
projects[admin_views][subdir] = "contrib"

projects[adminimal_admin_menu][version] = "1.7"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.1"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.14"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.10"
projects[date][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[reroute_email][version] = "1.3"
projects[reroute_email][subdir] = "contrib"

projects[auto_entitylabel][version] = "1.4"
projects[auto_entitylabel][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[feeds][version] = "2.0-beta4"
projects[feeds][subdir] = "contrib"

projects[entityreference][version] = "1.5"
projects[entityreference][subdir] = "contrib"

projects[field_group][version] = "1.6"
projects[field_group][subdir] = "contrib"

projects[field_permissions][version] = "1.0"
projects[field_permissions][subdir] = "contrib"

projects[mailsystem][version] = "2.34"
projects[mailsystem][subdir] = "contrib"

projects[simplemenu][version] = "1.x-dev"
projects[simplemenu][subdir] = "contrib"

projects[backup_migrate][version] = "3.5"
projects[backup_migrate][subdir] = "contrib"

projects[content_type_extras][version] = "1.13"
projects[content_type_extras][subdir] = "contrib"

projects[entity][version] = "1.9"
projects[entity][subdir] = "contrib"

projects[job_scheduler][version] = "2.0"
projects[job_scheduler][subdir] = "contrib"

projects[shs][version] = "1.6"
projects[shs][subdir] = "contrib"

projects[token][version] = "1.7"
projects[token][subdir] = "contrib"

projects[unique_field][version] = "1.0-rc1"
projects[unique_field][subdir] = "contrib"

projects[urllogin][version] = "1.x-dev"
projects[urllogin][subdir] = "contrib"

projects[userprotect][version] = "1.2"
projects[userprotect][subdir] = "contrib"

projects[panels][version] = "3.9"
projects[panels][subdir] = "contrib"

projects[rules][version] = "2.10"
projects[rules][subdir] = "contrib"

projects[rules_conditional][version] = "1.0-beta2"
projects[rules_conditional][subdir] = "contrib"

projects[rules_link][version] = "2.0"
projects[rules_link][subdir] = "contrib"

projects[page_title][version] = "2.7"
projects[page_title][subdir] = "contrib"

projects[better_exposed_filters][version] = "3.5"
projects[better_exposed_filters][subdir] = "contrib"

projects[views][version] = "3.18"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.4"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_field_view][version] = "1.2"
projects[views_field_view][subdir] = "contrib"

projects[views_term_hierarchy_weight_field][version] = "1.7"
projects[views_term_hierarchy_weight_field][subdir] = "contrib"

projects[views_tree][version] = "2.0"
projects[views_tree][subdir] = "contrib"


; +++++ Themes +++++

; responsive_bartik
projects[responsive_bartik][type] = "theme"
projects[responsive_bartik][version] = "1.0"
projects[responsive_bartik][subdir] = "contrib"

